#!/usr/bin/python

import MySQLdb

from helper import validation_nik, create_report
from sql_execution import create_table

db = MySQLdb.connect("localhost", "root", "", "DATA_SISWA")

umur_tup = (10, 15, 16, 18)
list_data_siswa = [
    {
        "nik": 10,
        "nama": "Asep",
        "alamat": "Bandung"
    },
    {
        "nik": 11,
        "nama": "Dadang",
        "alamat": "Jakarta"
    },
    {
        "nik": 12,
        "nama": "Joko",
        "alamat": "Bandung"
    },
    {
        "nik": 13,
        "nama": "Dondy",
        "alamat": "Yogyakarta"
    }
]

list_data_orang_tua = [
    {
        "nik_siswa": 12,
        "ibu": "Sulastri",
        "ayah": "Jajang"
    },
    {
        "nik_siswa": 10,
        "ibu": "Listia",
        "ayah": "Ujang"
    },
    {
        "nik_siswa": 11,
        "ibu": "Susi",
        "ayah": "Odang"
    },
    {
        "nik_siswa": 13,
        "ibu": "Santi",
        "ayah": "Tatang"
    }
]

list_new_data = []
count_loop = 0

table_siswa = """CREATE TABLE SISWA (
         NIK INT,
         NAMA  CHAR(50) NOT NULL,
         ALAMAT  CHAR(50),
         UMUR INT,
         AYAH CHAR(50),
         IBU CHAR(50))"""


if __name__ == '__main__':
    for item in list_data_siswa:
        item.update({'umur': umur_tup[count_loop]})
        data = validation_nik(
            obj_siswa=item, list_ortu=list_data_orang_tua)
        list_new_data.append(data)
        count_loop += 1

    create_report(data_list=list_new_data)
    create_table(db=db, sql_query=table_siswa)
