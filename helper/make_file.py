def create_report(data_list=None):
    new_file = open('report_siswa.txt', 'wb+')

    for item in data_list:
        if item.has_key('orang_tua'):
            data = """
            Data Siswa:
            Nik: {nk}
            Nama: {n}
            Alamat: {a}
            Umur: {u}

            Data orant tua:
            Nama Ayah: {oa}
            Nama Ibu: {oi}

            ----------------
            """.format(
                nk=item.get('nik'),
                n=item.get('nama'),
                a=item.get('alamat'),
                u=item.get('umur'),
                oa=item.get('orang_tua').get('ayah'),
                oi=item.get('orang_tua').get('ibu')
            )
            new_file.writelines(data)

    return new_file.close()
