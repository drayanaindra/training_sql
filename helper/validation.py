# Ini adalah library untuk memvalidasi data
# Seperti melakukan validasi nik siswa
# validasi nik karyawan dan lainnya


def validation_nik(obj_siswa=None, list_ortu=None):
    for item in list_ortu:
        if obj_siswa.get('nik') == item.get('nik_siswa'):
            item.pop('nik_siswa')
            obj_siswa.update({
                'orang_tua': item
            })

    return obj_siswa
